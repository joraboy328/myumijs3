
# umi project
# config/routes.js 路由配置里如果设定了name属性，console里就会报错
<!--#config/routes.js# export default [
   { 
      path: '/', 
      name: '首页', //不设置name属性就不报错了
      component: '@/pages/index', 
      icon: 'HomeOutlined',

   }
] -->
## Getting Started

Install dependencies,

```bash
$ yarn
```

Start the dev server,

```bash
$ yarn start
```
