import { defineConfig } from 'umi';
import routes from './routes.js';


export default defineConfig({

  nodeModulesTransform: {
    type: 'none',
  },

  antd:{

  },
  dva:{
    immer:false
  },


  layout:{

    name:'MYUMIJS3',
    locale:true,

  },
  routes,

  fastRefresh: {},
});
